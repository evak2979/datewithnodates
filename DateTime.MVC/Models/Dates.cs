﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DateTime.MVC.Models
{
    public class Dates
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public int DaysDifference { get; set; }
    }
}