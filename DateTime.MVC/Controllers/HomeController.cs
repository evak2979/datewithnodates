﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DateTime.Infrastructure.Concretes;
using DateTime.Infrastructure.Interfaces;
using DateTime.MVC.Models;

namespace DateTime.MVC.Controllers
{
    public class HomeController : Controller
    {
        private IDaysCalculator daysCalculator;
        private IDateStringSplitter dateStringSplitter;

        public HomeController()
        {
            // Not using a DI here - Setting up Autofac or Structuremap would add extra time in building this solution.
            // If you wish me to though, I can setup Startup accordingly post-review.
            // I have also avoided adding string input validation as I have been told to keep the time building this to a minimum.
            // If you require me to add validation I'm glad to do so as well.

            daysCalculator = new DaysCalculator(new IDaysRangeCalculator[]
            {
                //Order matters here - DI would sort this out.
                new DaysInYearRangeCalculator(new LeapYearCalculator()),
                new DaysInMonthRangeCalculator(new MonthsFactory(new LeapYearCalculator()), new RangeOfMonthsCalculator()),
                new LeftoverDaysInMonthRangeCalculator(new MonthsFactory(new LeapYearCalculator()))
            });

            dateStringSplitter = new YyyymmdDateStringSplitter();
        }

        [HttpGet]
        public ViewResult Index()
        {
            return View("Index", new Dates());
        }

        [HttpPost]
        public ViewResult Index(Dates datesModel)
        {
            var startDayMonthYear = this.dateStringSplitter.Create(datesModel.StartDate);
            var endDayMonthYear = this.dateStringSplitter.Create(datesModel.EndDate);

            datesModel.DaysDifference = this.daysCalculator.Calculate(startDayMonthYear, endDayMonthYear);

            return View("Index", datesModel);
        }
    }
}