﻿using System;
using DateTime.Infrastructure.Interfaces;

namespace DateTime.Infrastructure.Concretes
{
    public class LeapYearCalculator : ILeapYearCalculator
    {
        private int leapYear = 2000;

        public bool IsLeapYear(int year)
        {
            return Math.Abs(leapYear - year) % 4 == 0;
        }
    }
}