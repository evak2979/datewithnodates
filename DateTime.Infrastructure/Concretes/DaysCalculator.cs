﻿using System.Linq;
using DateTime.Infrastructure.Interfaces;

namespace DateTime.Infrastructure.Concretes
{
    public class DaysCalculator : IDaysCalculator
    {
        private readonly IDaysRangeCalculator[] _daysRangeCalculators;

        public DaysCalculator(IDaysRangeCalculator[] daysRangeCalculators)
        {
            _daysRangeCalculators = daysRangeCalculators;
        }

        public int Calculate(DayMonthYear start, DayMonthYear end)
        {
            return _daysRangeCalculators.Select(x => x.GetDaysRange(start, end)).Sum();
        }
    }
}