﻿using DateTime.Infrastructure.Interfaces;

namespace DateTime.Infrastructure.Concretes
{
    public class DayMonthYearProcessor : IDayMonthYearProcessor
    {
        public int GetYearMonthDay(DayMonthYear dayMonthYear)
        {
            return int.Parse(dayMonthYear.Year.ToString() + dayMonthYear.Month.ToString() +
                             dayMonthYear.Day.ToString());
        }

        public int GetMonthDay(DayMonthYear dayMonthYear)
        {
            return int.Parse(dayMonthYear.Month.ToString() +
                             dayMonthYear.Day.ToString());
        }

        public int GetDay(DayMonthYear dayMonthYear)
        {
            return int.Parse(dayMonthYear.Day.ToString());
        }
    }
}