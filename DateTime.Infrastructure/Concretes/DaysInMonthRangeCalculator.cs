﻿using System.Collections.Generic;
using System.Linq;
using DateTime.Infrastructure.Interfaces;
using DateTime.Infrastructure.Months;

namespace DateTime.Infrastructure.Concretes
{
    public class DaysInMonthRangeCalculator : IDaysRangeCalculator
    {
        private readonly IMonthsFactory _monthsFactory;
        private readonly IMonthRangeCalculator _monthRangeCalculator;
        private Dictionary<int, BaseMonth> Months { get; }

        public DaysInMonthRangeCalculator(IMonthsFactory monthsFactory, IMonthRangeCalculator monthRangeCalculator)
        {
            _monthsFactory = monthsFactory;
            _monthRangeCalculator = monthRangeCalculator;

            Months = _monthsFactory.Create();
        }

        public int GetDaysRange(DayMonthYear start, DayMonthYear end)
        {
            var rangeOfMonths = this._monthRangeCalculator.GetDaysRange(start, end);

            var numberOfDays = 0;

            rangeOfMonths.ToList().ForEach(x => numberOfDays += Months[x.Month].GetNumberOfDays(x));

            return numberOfDays;
        }
    }
}
