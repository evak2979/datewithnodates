﻿using DateTime.Infrastructure.Interfaces;

namespace DateTime.Infrastructure.Concretes
{
    public class DaysInYearRangeCalculator : IDaysRangeCalculator
    {
        private readonly ILeapYearCalculator _leapYearCalculator;

        public DaysInYearRangeCalculator(ILeapYearCalculator leapYearCalculator)
        {
            _leapYearCalculator = leapYearCalculator;
        }

        public int GetDaysRange(DayMonthYear start, DayMonthYear end)
        {
            var sumOfDays = 0;

            for (int i = start.Year; i < end.Year; i++)
            {
                if (i + 1 == end.Year && start.Month > end.Month)
                    break;

                if (_leapYearCalculator.IsLeapYear(i))
                    sumOfDays += 366;
                else
                    sumOfDays += 365;

                start.Year++;
            }

            return sumOfDays;
        }
    }
}