﻿using DateTime.Infrastructure.Interfaces;

namespace DateTime.Infrastructure.Concretes
{
    public class LeftoverDaysInMonthRangeCalculator : IDaysRangeCalculator
    {
        private readonly IMonthsFactory _monthsFactory;

        public LeftoverDaysInMonthRangeCalculator(IMonthsFactory monthsFactory)
        {
            _monthsFactory = monthsFactory;
        }

        public int GetDaysRange(DayMonthYear start, DayMonthYear end)
        {
            if (start.Month != end.Month)
            {
                return _monthsFactory.Create()[start.Month].GetNumberOfDays(start) - start.Day + end.Day;
            }
            else
            {
                return end.Day - start.Day;
            }
        }
    }
}