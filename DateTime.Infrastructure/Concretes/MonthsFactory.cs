﻿using System.Collections.Generic;
using DateTime.Infrastructure.Interfaces;
using DateTime.Infrastructure.Months;

namespace DateTime.Infrastructure.Concretes
{
    public class MonthsFactory : IMonthsFactory
    {
        private readonly ILeapYearCalculator _leapYearCalculator;

        public MonthsFactory(ILeapYearCalculator leapYearCalculator)
        {
            _leapYearCalculator = leapYearCalculator;
        }

        public Dictionary<int, BaseMonth> Create()
        {
            Dictionary<int, BaseMonth> Months = new Dictionary<int, BaseMonth>();

            Months.Add(1, new January());
            Months.Add(2, new February(_leapYearCalculator));
            Months.Add(3, new March());
            Months.Add(4, new April());
            Months.Add(5, new May());
            Months.Add(6, new June());
            Months.Add(7, new July());
            Months.Add(8, new August());
            Months.Add(9, new September());
            Months.Add(10, new October());
            Months.Add(11, new November());
            Months.Add(12, new December());

            return Months;
        }
    }
}