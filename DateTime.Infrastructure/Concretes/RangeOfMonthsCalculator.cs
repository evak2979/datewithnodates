﻿using System.Collections.Generic;
using DateTime.Infrastructure.Interfaces;

namespace DateTime.Infrastructure.Concretes
{
    public class RangeOfMonthsCalculator : IMonthRangeCalculator
    {
        public List<DayMonthYear> GetDaysRange(DayMonthYear start, DayMonthYear end)
        {
            List<DayMonthYear> listOfDayMonthYears = new List<DayMonthYear>();

            if (start.Month == end.Month)
                return listOfDayMonthYears;

            if (start.Month < end.Month)
            {
                for (int i = start.Month + 1; i < end.Month; i++)
                {
                    listOfDayMonthYears.Add(new DayMonthYear
                    {
                        Month = i,
                        Year = start.Year
                    });
                }
            }
            else
            {
                for (int i = start.Month + 1; i <= 12; i++)
                {
                    listOfDayMonthYears.Add(new DayMonthYear
                    {
                        Month = i,
                        Year = start.Year
                    });
                }

                for (int i = 1; i < end.Month; i++)
                {
                    listOfDayMonthYears.Add(new DayMonthYear
                    {
                        Month = i,
                        Year = end.Year
                    });
                }
            }

            return listOfDayMonthYears;
        }
    }
}