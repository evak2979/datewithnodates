﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public class May : BaseMonth
    {
        public May()
        {
            NumberOfDays = 31;
        }
    }
}