﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public class January : BaseMonth
    {
        public January()
        {
            NumberOfDays = 31;
        }
    }
}