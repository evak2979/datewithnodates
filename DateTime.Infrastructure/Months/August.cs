﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public class August : BaseMonth
    {
        public August()
        {
            NumberOfDays = 31;
        }
    }
}