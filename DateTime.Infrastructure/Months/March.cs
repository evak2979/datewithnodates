﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public class March : BaseMonth
    {
        public March()
        {
            NumberOfDays = 31;
        }
    }
}