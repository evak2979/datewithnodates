﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public class October : BaseMonth
    {
        public October()
        {
            NumberOfDays = 31;
        }
    }
}