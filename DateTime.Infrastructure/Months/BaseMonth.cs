﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public abstract class BaseMonth
    {
        protected int NumberOfDays { get; set; }

        public virtual int GetNumberOfDays(DayMonthYear year = null)
        {
            return NumberOfDays;
        }
    }
}
