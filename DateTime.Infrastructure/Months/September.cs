﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public class September : BaseMonth
    {
        public September()
        {
            NumberOfDays = 30;
        }
    }
}