﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public class November : BaseMonth
    {
        public November()
        {
            NumberOfDays = 30;
        }
    }
}