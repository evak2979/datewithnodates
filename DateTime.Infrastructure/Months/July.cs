﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public class July : BaseMonth
    {
        public July()
        {
            NumberOfDays = 31;
        }
    }
}