﻿using DateTime.Infrastructure.Interfaces;

namespace DateTime.Infrastructure.Months
{
    public class February : BaseMonth
    {
        private readonly ILeapYearCalculator _leapYearCalculator;

        public February(ILeapYearCalculator leapYearCalculator)
        {
            _leapYearCalculator = leapYearCalculator;
            NumberOfDays = 28;
        }

        public override int GetNumberOfDays(DayMonthYear dayMonthYear)
        {
            if (_leapYearCalculator.IsLeapYear(dayMonthYear.Year))
                return NumberOfDays + 1;

            return NumberOfDays;
        }
    }
}