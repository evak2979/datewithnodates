﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public class December : BaseMonth
    {
        public December()
        {
            NumberOfDays = 31;
        }
    }
}