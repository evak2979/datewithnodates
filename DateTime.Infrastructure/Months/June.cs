﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public class June : BaseMonth
    {
        public June()
        {
            NumberOfDays = 30;
        }
    }
}