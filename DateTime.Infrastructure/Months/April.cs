﻿using System.Diagnostics.CodeAnalysis;

namespace DateTime.Infrastructure.Months
{
    [ExcludeFromCodeCoverage]
    public class April : BaseMonth
    {
        public April()
        {
            NumberOfDays = 30;
        }
    }
}