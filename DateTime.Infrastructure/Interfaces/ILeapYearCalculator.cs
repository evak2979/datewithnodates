﻿namespace DateTime.Infrastructure.Interfaces
{
    public interface ILeapYearCalculator
    {
        bool IsLeapYear(int year);
    }
}