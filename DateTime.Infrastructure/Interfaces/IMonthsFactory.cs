﻿using System.Collections.Generic;
using DateTime.Infrastructure.Months;

namespace DateTime.Infrastructure.Interfaces
{
    public interface IMonthsFactory
    {
        Dictionary<int, BaseMonth> Create();
    }
}