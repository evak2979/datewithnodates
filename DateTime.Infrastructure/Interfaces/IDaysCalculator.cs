﻿namespace DateTime.Infrastructure.Interfaces
{
    public interface IDaysCalculator
    {
        int Calculate(DayMonthYear start, DayMonthYear end);
    }
}