﻿namespace DateTime.Infrastructure.Interfaces
{
    public interface IDateStringSplitter
    {
        DayMonthYear Create(string date);
    }

    public class YyyymmdDateStringSplitter : IDateStringSplitter
    {
        public DayMonthYear Create(string date)
        {
            var dateParts = GetDateParts(date);

            return new DayMonthYear
            {
                Year = int.Parse(dateParts[0]),
                Month = int.Parse(dateParts[1]),
                Day = int.Parse(dateParts[2])
            };
        }

        private string[] GetDateParts(string date)
        {
            return date.Split('/');
        }
    }
}
