﻿using System.Collections.Generic;

namespace DateTime.Infrastructure.Interfaces
{
    public interface IMonthRangeCalculator
    {
        List<DayMonthYear> GetDaysRange(DayMonthYear start, DayMonthYear end);
    }
}