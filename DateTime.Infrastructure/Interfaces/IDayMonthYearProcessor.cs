﻿namespace DateTime.Infrastructure.Interfaces
{
    public interface IDayMonthYearProcessor
    {
        int GetYearMonthDay(DayMonthYear dayMonthYear);

        int GetMonthDay(DayMonthYear dayMonthYear);

        int GetDay(DayMonthYear dayMonthYear);
    }
}