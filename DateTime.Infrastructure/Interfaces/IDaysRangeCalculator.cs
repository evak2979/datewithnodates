﻿namespace DateTime.Infrastructure.Interfaces
{
    public interface IDaysRangeCalculator
    {
        int GetDaysRange(DayMonthYear start, DayMonthYear end);
    }
}