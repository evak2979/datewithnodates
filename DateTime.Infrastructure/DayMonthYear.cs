﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DateTime.Infrastructure
{

    public class DayMonthYear
    {
        public int Year { get; set; }

        public int Month { get; set; }

        public int Day { get; set; }
    }
}
