﻿using System.Collections.Generic;
using System.Linq;
using DateTime.Infrastructure;
using DateTime.Infrastructure.Concretes;
using DateTime.Infrastructure.Interfaces;
using DateTime.Infrastructure.Months;
using Moq;
using NUnit.Framework;
using Shouldly;

namespace DateTime.Tests.Unit
{
    public class DaysInMonthRangeCalculatorTests
    {
        private class StubMonth : BaseMonth
        {
            public override int GetNumberOfDays(DayMonthYear year = null)
            {
                return 1;
            }
        }

        private DaysInMonthRangeCalculator subject;
        private Mock<IMonthsFactory> mockMonthsFactory;
        private Mock<IMonthRangeCalculator> mockMonthRangeCalculator;
        private Dictionary<int, BaseMonth> monthsDictionary;

        [SetUp]
        public void Setup()
        {
            mockMonthsFactory = new Mock<IMonthsFactory>();
            mockMonthRangeCalculator = new Mock<IMonthRangeCalculator>();
            monthsDictionary = new Dictionary<int, BaseMonth>();
            monthsDictionary.Add(2, new StubMonth());
            monthsDictionary.Add(3, new StubMonth());
            monthsDictionary.Add(4, new StubMonth());

            mockMonthsFactory.Setup(x => x.Create()).Returns(monthsDictionary);

            subject = new DaysInMonthRangeCalculator(mockMonthsFactory.Object, mockMonthRangeCalculator.Object);
        }

        [Test]
        public void ShouldCalculateTotalDaysInMonthRangeCorrectly()
        {
            // setup
            mockMonthRangeCalculator.Setup(x => x.GetDaysRange(It.IsAny<DayMonthYear>(), It.IsAny<DayMonthYear>()))
                .Returns(new List<DayMonthYear>
            {
                new DayMonthYear
                {
                    Month = 2,
                    Year = 2001
                },
                new DayMonthYear
                {
                    Month = 3,
                    Year = 2001
                },
                new DayMonthYear
                {
                    Month = 4,
                    Year = 2001
                }
            });
            

            // act
            var result = subject.GetDaysRange(new DayMonthYear(), new DayMonthYear());

            // assert
            result.ShouldBe(monthsDictionary.ToList().Sum(x => x.Value.GetNumberOfDays()));
        }
    }
}
