﻿using DateTime.Infrastructure.Concretes;
using DateTime.Infrastructure.Months;
using NUnit.Framework;
using Shouldly;

namespace DateTime.Tests.Unit
{
    public class LeapYearCalculatorTests
    {
        private LeapYearCalculator _subject;

        [SetUp]
        public void Setup()
        {
            _subject = new LeapYearCalculator();
        }

        [TestCase(2000)]
        [TestCase(2004)]
        [TestCase(2008)]
        [TestCase(2012)]
        [TestCase(2016)]
        [TestCase(2024)]
        public void ShouldReturnTrueForLeapYears(int year)
        {
            // setup + act + asset
            _subject.IsLeapYear(year).ShouldBeTrue();
        }

        [TestCase(2001)]
        [TestCase(2003)]
        [TestCase(2005)]
        [TestCase(1999)]
        [TestCase(2015)]
        [TestCase(2021)]
        public void ShouldReturnFalseForNonLeapYears(int year)
        {
            // setup + act + asset
            _subject.IsLeapYear(year).ShouldBeFalse();
        }
    }
}
