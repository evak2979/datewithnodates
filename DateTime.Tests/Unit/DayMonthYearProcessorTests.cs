﻿using DateTime.Infrastructure;
using DateTime.Infrastructure.Concretes;
using NUnit.Framework;
using Shouldly;

namespace DateTime.Tests.Unit
{
    public class DayMonthYearProcessorTests
    {
        private DayMonthYearProcessor subject;

        [SetUp]
        public void Setup()
        {
            this.subject = new DayMonthYearProcessor();
        }

        [Test]
        public void ShouldReturnTheYearMonthDayFormatCorrectly()
        {
            // setup
            var dayMonthYear = new DayMonthYear
            {
                Day = 1,
                Month = 2,
                Year = 3456
            };

            // act
            var result = subject.GetYearMonthDay(dayMonthYear);

            // assert
            result.ShouldBe(345621);
        }

        [Test]
        public void ShouldReturnTheMonthDayFormatCorrectly()
        {
            // setup
            var dayMonthYear = new DayMonthYear
            {
                Day = 1,
                Month = 2,
                Year = 3456
            };

            // act
            var result = subject.GetMonthDay(dayMonthYear);

            // assert
            result.ShouldBe(21);
        }

        [Test]
        public void ShouldReturnTheDayFormatCorrectly()
        {
            // setup
            var dayMonthYear = new DayMonthYear
            {
                Day = 1,
                Month = 2,
                Year = 3456
            };

            // act
            var result = subject.GetDay(dayMonthYear);

            // assert
            result.ShouldBe(1);
        }
    }
}
