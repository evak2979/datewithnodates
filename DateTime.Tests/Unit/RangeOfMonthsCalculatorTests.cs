﻿using System.Linq;
using DateTime.Infrastructure;
using DateTime.Infrastructure.Concretes;
using NUnit.Framework;
using Shouldly;

namespace DateTime.Tests.Unit
{
    public class RangeOfMonthsCalculatorTests
    {
        private RangeOfMonthsCalculator subject;

        [SetUp]
        public void Setup()
        {
            subject = new RangeOfMonthsCalculator();
        }

        [Test]
        public void ShouldReturnRegularOrderWhenStartMonthSmallerThanEndMonth()
        {
            // setup
            var dayMonthYearStart = new DayMonthYear
            {
                Day = 1,
                Month = 1,
                Year = 2000
            };
            var dayMonthYearEnd = new DayMonthYear
            {
                Day = 1,
                Month = 12,
                Year = 2000
            };

            // act
            var result = subject.GetDaysRange(dayMonthYearStart, dayMonthYearEnd);

            // assert
            string.Join("", result.Select(x => x.Month.ToString())).ShouldBe("234567891011");
            result.All(x => x.Year == dayMonthYearStart.Year).ShouldBeTrue();
        }

        [Test]
        public void ShouldResetTheCounterAfterTwelveMOnthWhenStartMonthGreaterThanEndMonth()
        {
            // setup
            var dayMonthYearStart = new DayMonthYear
            {
                Day = 1,
                Month = 8,
                Year = 2000
            };
            var dayMonthYearEnd = new DayMonthYear
            {
                Day = 1,
                Month = 6,
                Year = 2001
            };

            // act
            var result = subject.GetDaysRange(dayMonthYearStart, dayMonthYearEnd);

            // assert
            string.Join("", result.Select(x => x.Month.ToString())).ShouldBe("910111212345");
            result.Where(x => x.Year >= 8 && x.Year <= 12).All(x => x.Year == dayMonthYearStart.Year).ShouldBeTrue();
            result.Where(x => x.Year >= 1 && x.Year <= 6).All(x => x.Year == dayMonthYearEnd.Year).ShouldBeTrue();
        }

        [Test]
        public void ShouldReturnEmptyArrayForEqualMonths()
        {
            // setup
            var dayMonthYearStart = new DayMonthYear
            {
                Month = 1
            };
            var dayMonthYearEnd = new DayMonthYear
            {
                Month = 1
            };

            // // act
            var result = subject.GetDaysRange(dayMonthYearStart, dayMonthYearEnd);

            // assert
            result.ShouldBeEmpty();
        }
    }
}
