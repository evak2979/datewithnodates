﻿using System.Collections.Generic;
using DateTime.Infrastructure;
using DateTime.Infrastructure.Concretes;
using DateTime.Infrastructure.Interfaces;
using DateTime.Infrastructure.Months;
using Moq;
using NUnit.Framework;
using Shouldly;

namespace DateTime.Tests.Unit
{
    public class LeftoverDaysInMonthRangeCalculatorTests
    {
        private LeftoverDaysInMonthRangeCalculator subject;
        private Mock<IMonthsFactory> mockMonthsFactory;
        private Dictionary<int, BaseMonth> stubDictionary;

        [SetUp]
        public void Setup()
        {
            stubDictionary = new Dictionary<int, BaseMonth>();
            stubDictionary.Add(1, new MonthStub());
            stubDictionary.Add(2, new MonthStub());

            mockMonthsFactory = new Mock<IMonthsFactory>();
            mockMonthsFactory.Setup(x => x.Create()).Returns(stubDictionary);

            subject = new LeftoverDaysInMonthRangeCalculator(mockMonthsFactory.Object);
        }

        [Test]
        public void ShouldReturnTheSumOfDaysFromStartDayToFinalDayOfStartMonthAndFirstDayOfEndMonthToEndDate()
        {
            // setup
            DayMonthYear start = new DayMonthYear
            {
                Month = 1,
                Day = 10
            };

            DayMonthYear end = new DayMonthYear
            {
                Month = 2,
                Day = 10
            };

            // act
            var result = subject.GetDaysRange(start, end);

            // assert
            result.ShouldBe(30);
        }

        private class MonthStub : BaseMonth
        {
            public MonthStub()
            {
                this.NumberOfDays = 30;
            }
        }
    }
}
