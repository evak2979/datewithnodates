﻿using DateTime.Infrastructure.Interfaces;
using NUnit.Framework;
using Shouldly;

namespace DateTime.Tests.Unit
{
    public class YYYYMMDDateStringSplitterTests
    {
        private YyyymmdDateStringSplitter _subject;

        [SetUp]
        public void Setup()
        {
            _subject = new YyyymmdDateStringSplitter();
        }

        [Test]
        public void ShouldPopulateDayMonthYearProperly()
        {
            // setup + act
            var result = _subject.Create("2010/05/21");

            // assert
            result.Year.ShouldBe(2010);
            result.Month.ShouldBe(5);
            result.Day.ShouldBe(21);
        }
    }
}
