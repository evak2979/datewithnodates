﻿using DateTime.Infrastructure;
using DateTime.Infrastructure.Concretes;
using DateTime.Infrastructure.Interfaces;
using Moq;
using NUnit.Framework;
using Shouldly;

namespace DateTime.Tests.Unit
{
    public class DaysInYearRangeCalculatorTests
    {
        private DaysInYearRangeCalculator subject;
        private Mock<ILeapYearCalculator> mockLeapYearCalculator;

        [SetUp]
        public void Setup()
        {
            this.mockLeapYearCalculator = new Mock<ILeapYearCalculator>();

            this.subject = new DaysInYearRangeCalculator(this.mockLeapYearCalculator.Object);
        }

        [Test]
        public void ShouldReturnCorrectNonLeapYearNumberOfDays()
        {
            // setup
            mockLeapYearCalculator.Setup(x => x.IsLeapYear(It.IsAny<int>())).Returns(false);
            DayMonthYear start = new DayMonthYear()
            {
                Year = 2000
            };
            DayMonthYear end = new DayMonthYear()
            {
                Year = 2002
            };

            // act
            var result = subject.GetDaysRange(start, end);

            // assert
            result.ShouldBe(730);
        }

        [Test]
        public void ShouldSetStartYearToEndYearByEndOfCalculation()
        {
            // setup
            mockLeapYearCalculator.Setup(x => x.IsLeapYear(It.IsAny<int>())).Returns(false);
            DayMonthYear start = new DayMonthYear()
            {
                Year = 2000
            };
            DayMonthYear end = new DayMonthYear()
            {
                Year = 2002
            };

            // act
            subject.GetDaysRange(start, end);

            // assert
            start.Year.ShouldBe(end.Year);
        }

        [Test]
        public void ShouldReturnZeroDaysIfDateBetweenStartAndEndYearLessThanAYear()
        {
            // setup
            mockLeapYearCalculator.Setup(x => x.IsLeapYear(2000)).Returns(true);
            DayMonthYear start = new DayMonthYear()
            {
                Year = 2000,
                Month = 10
            };
            DayMonthYear end = new DayMonthYear()
            {
                Year = 2001,
                Month = 9
            };

            // act
            var result = subject.GetDaysRange(start, end);

            // assert
            result.ShouldBe(0);
        }

        [Test]
        public void ShouldReturnCorrectLeapYearNumberOfDays()
        {
            // setup
            mockLeapYearCalculator.Setup(x => x.IsLeapYear(2000)).Returns(true);
            DayMonthYear start = new DayMonthYear()
            {
                Year = 2000
            };
            DayMonthYear end = new DayMonthYear()
            {
                Year = 2002
            };

            // act
            var result = subject.GetDaysRange(start, end);

            // assert
            result.ShouldBe(731);
        }
    }
}
