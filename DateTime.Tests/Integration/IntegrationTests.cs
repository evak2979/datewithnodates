﻿using DateTime.Infrastructure;
using DateTime.Infrastructure.Concretes;
using DateTime.Infrastructure.Interfaces;
using DateTime.Infrastructure.Months;
using NUnit.Framework;
using Shouldly;

namespace DateTime.Tests.Integration
{
    public class IntegrationTests
    {
        private DaysCalculator subject;

        [SetUp]
        public void Setup()
        {
            subject = new DaysCalculator(new IDaysRangeCalculator[]
            {
                //Order matters here - DI would sort this out.
                new DaysInYearRangeCalculator(new LeapYearCalculator()),
                new DaysInMonthRangeCalculator(new MonthsFactory(new LeapYearCalculator()), new RangeOfMonthsCalculator()),
                new LeftoverDaysInMonthRangeCalculator(new MonthsFactory(new LeapYearCalculator()))
            });
        }

        [Test]

        public void DaysBetweenSameMonthAndYearShouldBeCorrect()
        {
            // setup
            DayMonthYear start = new DayMonthYear
            {
                Month = 1,
                Day = 10,
                Year = 2000
            };

            DayMonthYear end = new DayMonthYear
            {
                Month = 1,
                Day = 20,
                Year = 2000
            };

            // act
            var result = subject.Calculate(start, end);

            // assert
            result.ShouldBe(10);
        }

        [Test]

        public void DaysBetweenSubsequentMonthAndSameYearShouldBeCorrect()
        {
            // setup
            DayMonthYear start = new DayMonthYear
            {
                Month = 1,
                Day = 10,
                Year = 2000
            };

            DayMonthYear end = new DayMonthYear
            {
                Month = 2,
                Day = 10,
                Year = 2000
            };

            // act
            var result = subject.Calculate(start, end);

            // assert
            result.ShouldBe(31);
        }

        [Test]

        public void DaysBetweenSameDayAndMonthAndNonLeapYearsShouldBe365()
        {
            // setup
            DayMonthYear start = new DayMonthYear
            {
                Month = 1,
                Day = 1,
                Year = 2001
            };

            DayMonthYear end = new DayMonthYear
            {
                Month = 1,
                Day = 1,
                Year = 2002
            };

            // act
            var result = subject.Calculate(start, end);

            // assert
            result.ShouldBe(365);
        }

        [Test]

        public void DaysBetweenSameDayAndMonthAndLeapYearsShouldBe366()
        {
            // setup
            DayMonthYear start = new DayMonthYear
            {
                Month = 1,
                Day = 1,
                Year = 2000
            };

            DayMonthYear end = new DayMonthYear
            {
                Month = 1,
                Day = 1,
                Year = 2001
            };

            // act
            var result = subject.Calculate(start, end);

            // assert
            result.ShouldBe(366);
        }

        [Test]

        public void DaysBetweenDifferentDaysAndDifferentMonthsAndDifferentYearsShouldBeCorrectForNonLeapYears()
        {
            // setup
            // Datetime for range check purposes for testing purposes
            var startDt = new System.DateTime(2001, 1, 1);
            var endDt = new System.DateTime(2002, 3, 3);
            var difference = (endDt - startDt).Days;

            DayMonthYear start = new DayMonthYear
            {
                Month = 1,
                Day = 1,
                Year = 2001
            };

            DayMonthYear end = new DayMonthYear
            {
                Month = 3,
                Day = 3,
                Year = 2002
            };

            // act
            var result = subject.Calculate(start, end);

            // assert
            result.ShouldBe(difference);
        }

        [Test]

        public void DaysBetweenDifferentDaysAndDifferentMonthsAndDifferentYearsShouldBeCorrectForLeapYears()
        {
            // setup
            // Datetime for range check purposes for testing purposes
            var startDt = new System.DateTime(2000, 1, 1);
            var endDt = new System.DateTime(2001, 3, 3);
            var difference = (endDt - startDt).Days;

            DayMonthYear start = new DayMonthYear
            {
                Month = 1,
                Day = 1,
                Year = 2000
            };

            DayMonthYear end = new DayMonthYear
            {
                Month = 3,
                Day = 3,
                Year = 2001
            };

            // act
            var result = subject.Calculate(start, end);

            // assert
            result.ShouldBe(difference);
        }
    }
}
